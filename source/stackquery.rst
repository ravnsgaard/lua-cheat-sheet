Inspecting the stack
====================

.. c:function::
   int lua_isboolean(lua_State*, int)
   int lua_iscfunction(lua_State*, int)
   int lua_isfunction(lua_State*, int)
   int lua_islightuserdata(lua_State*, int)
   int lua_isnil(lua_State*, int)
   int lua_isnone(lua_State*, int)
   int lua_isnoneornil(lua_State*, int)
   int lua_isnumber(lua_State*, int)
   int lua_isstring(lua_State*, int)
   int lua_istable(lua_State*, int)
   int lua_isthread(lua_State*, int)
   int lua_isuserdata(lua_State*, int)

   Query the stack value at the given index and return whether it is (``true``) 
   or is not (``false``) of the specified type. Note that standard Lua 
   coercions still apply, e.g. :c:func:`lua_isnumber()` returns ``true`` if the 
   value is a string that can be converted to a number. If this is undesirable, 
   use :c:func:`lua_type()`.

.. c:function::
   int lua_type(lua_State*, int)

   Returns the type of the value at the given index in the stack.
   This function is useful for switch statements and if string or 
   number must be determined without Lua's coercions.

   The return type is one of these constants:

   ================== =============
   LUA_TBOOLEAN       a boolean
   LUA_TFUNCTION      a function
   LUA_TLIGHTUSERDATA lightuserdata
   LUA_TNIL           nil
   LUA_TNONE          no value
   LUA_TSTRING        a string
   LUA_TTABLE         a table
   LUA_TTHREAD        a thread
   LUA_TUSERDATA      full userdata
   ================== =============
