Moving values from C onto the stack
===================================

.. c:function:: void lua_pushnil(lua_State*)

   Push ``nil`` onto the stack.

.. c:function:: void lua_pushboolean(lua_State*, int)

   Push an integer onto the stack as a boolean.
   A ``0`` is ``false``. Anything else is ``true``.

.. c:function:: void lua_pushnumber(lua_State*, lua_Number)

   Push a number onto the stack. ``lua_Number`` is ``double`` by default.

.. c:function:: void lua_pushinteger(lua_State*, lua_Integer)

   Push an integer onto the stack. ``lua_Integer`` is a signed integral large 
   enough to store the size of large strings. Typically it is ``ptrdiff_t``.

.. c:function:: void lua_pushunsigned(lua_State*, lua_Unsigned)

   Push an unsigned integer onto the stack. ``lua_Unsigned`` is a 32 bit 
   unsigned integral type.

.. c:function:: void lua_pushlstring(lua_State*, char const *, size_t)

   Push a string of given length onto the stack. The string can contain 
   arbitrary data.

.. c:function:: void lua_pushstring(lua_State*, char const *)

   Push a ``\0``-terminated string onto the stack.

