Moving values to C from the stack
=================================

.. c:function:: int lua_toboolean(lua_State*, int)

   Retrieve a boolean from the given index in the stack. Returns ``0`` or ``1`` 
   for ``false`` and ``true`` respectively. *Truth* is determined by Lua: 
   ``false`` and ``nil`` are ``false``; everything else is ``true``.

.. c:function:: char const* lua_tostring(lua_State*, int)

   Retrieve a string from the given index in the stack. Returns ``NULL`` if the 
   value at that index is not a string. No length is returned, but the string 
   pointed to is guaranteed to be ``\0``-terminated. The string is still stored 
   in immutable Lua space, so it is not allowed to modify it in any way.

.. c:function:: char const* lua_tolstring(lua_State*, int, size_t*)

   Retrieve a string from the given index in the stack. Returns ``NULL`` if the 
   value at that index is not a string. The ``size_t`` pointer returns the 
   length of the string. The string is still stored in immutable Lua space, so 
   it is not allowed to modify it in any way.

.. c:function::
   lua_Number lua_tonumber(lua_State*, int)
   lua_Integer lua_tointeger(lua_State*, int)
   lua_Unsigned lua_tounsigned(lua_State*, int)

   Retrieve a number from the given index in the stack. Returns ``0`` if the 
   value at that index is not a number.

.. c:function::
   lua_Number lua_tonumberx(lua_State*, int, int*)
   lua_Integer lua_tointegerx(lua_State*, int, int*)
   lua_Unsigned lua_tounsignedx(lua_State*, int, int*)

   Retrieve a number from the given index in the stack. The pointer is assigned 
   a boolean ``true`` (i.e. ``1``) if the value at that index is indeed a 
   number; otherwise it is assigned a boolean ``false`` (i.e. ``0``).
