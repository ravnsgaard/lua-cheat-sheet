Working directly with the stack
===============================

.. c:function:: void lua_arith(lua_State* L, int op)

   Performs an arithmetic operation over the two values (or one, in the case of 
   negation) at the top of the stack, with the value at the top being the 
   second operand, pops these values, and pushes the result of the operation. 
   The function follows the semantics of the corresponding Lua operator (that 
   is, it may call metamethods).

   The value of ``op`` must be one of the following constants:

   ========= ========================================
   LUA_OPADD performs addition (+)
   LUA_OPSUB performs subtraction (-)
   LUA_OPMUL performs multiplication (*)
   LUA_OPDIV performs division (/)
   LUA_OPMOD performs modulo (%)
   LUA_OPPOW performs exponentiation (^)
   LUA_OPUNM performs mathematical negation (unary -)
   ========= ========================================

lua_checkstack
lua_compare
lua_concat
lua_copy
lua_gettop
lua_insert
lua_len
lua_pop
lua_remove
lua_settop

lua_rawequal
lua_rawlen
